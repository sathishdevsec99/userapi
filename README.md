# User API
In this project, we are migrating exiting C# and Azure-based API to Node JS and AWS. 

## 1) System Requirement 

### docker-desktop version


## 2) Download & Build on local

### Clone the repository, install node packages  and verify routes locally
    git clone git@bitbucket.org:zee5in/zee5-userapi-node.git

### Run Below command for your usage.
    To start Application in local system without docker
        sh start_api.sh dev

    To start Application with docker
        sh docker_command.sh

    To run in Background without log file
        sh docker_command.sh 2>&1 &

    To run in Background With log file
        sh docker_command.sh >> ~/docker.log 2>&1 &

### Open your local browser and verify the user API is working by accessing:     
`http://localhost:3000/v1/healthCheck`

`http://localhost:3000/v1/login`

`http://localhost:3000/v1/*`   

## 3) Postman Collection


## 4) Code Branch and Environment 

	We have 3 New Infra setups for UserAPI.

	1. Stage
			Repo Branch name: stage
			URL: uapi-stage.zee5.com
	2. Preprod
			Repo Branch name: preprod
			URL: uapi-preprod.zee5.com
	3. Production
			Repo Branch name: prod
			URL: uapi.zee5.com

## 5) CR, Branching and Merge Process 

	All team members must download the latest code from the "stage" Branch and create their task branch to start working on it. 
	Ex: Task id is Z5BE-9020 then branch name will be z5be-9020

	In the end, all members must merge code progress on the "dev" branch, if the code is complete or half done. This will be a progressive branch to be reviewed by Biplab Sarkar.

	Once the Task code is finished it should align to one of Peer for CR and Test.

	The person who is doing CR and test must share feedback on the Jira ticket. 

	There will be 2 PR. Once everything is working fine. The developer has to again take the latest pull from the "stage" branch so that any conflict he/she can correct on their respective branch. After taking pull Developer need to test the code one more time to make sure everything is working fine. 

	Once CR, Test and Pull from the "stage" branch done, ask Release Manager to merge code in the "stage" branch.

	Release Manager: This is the person who will merge all developer code in stage and pre-prod. For every release, there would be different RM. 

