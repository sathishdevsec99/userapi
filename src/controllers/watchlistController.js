const businessLogic = require("../services/watchlist/watchlistService");
const response = require("../helpers/responseHelper");
const redisCluster = require("../connections/redisConnection");
const redisKey = process.env.REDIS_KEY;

// Get the watchlist of the current user
exports.getWatchlist = async (req, res, next) => {
    try {

        let result = await redisCluster.get(`${redisKey}${req.headers.userid}`);
        if (!result) {
            const resultAll = await businessLogic.getWatchlist(req, res);
            result = JSON.stringify(resultAll);
            await redisCluster.set(`${redisKey}${req.headers.userid}`, result);
        }
        result = dataProjection(result);
        res.status(200).send(result);
        // let apiResponse = response.generate(200, "Data found", result);

    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
    
};


// Add an item to the watchlist of the current user
exports.addWatchlist = async (req, res, next) => {
    try {
        const result = await businessLogic.addWatchlist(req, res);
        await redisCluster.del(`${redisKey}${req.headers.userid}`);
        if (result) {
            // let apiResponse = response.generate(1, "watchlist was added successfully");
           return  res.status(200).send({ "code":1,
            "message":"watchlist was added successfully"});
        }
    } catch (err) {
        if (!err.statusCode) {
            err.message = "Item couldn't be added (DB insert failure)";
            err.statusCode = 500;
        }
        next(err);
    }
};


// Update an item in the watchlist of the current user
exports.updateWatchlist = async (req, res, next) => {
    try {
        const result = await businessLogic.updateWatchlist(req, res);
        await redisCluster.del(`${redisKey}${req.headers.userid}`);
        if (result) {
        // let apiResponse = response.generate(1, "watchlist was updated successfully");
        return  res.status(200).send({ "code":1,
            "message":"watchlist was updated successfully"});
        }
    } catch (err) {
        if (!err.statusCode) {
            err.message = "Item couldn't be updated (DB update failure)";
            err.statusCode = 500;
        }
        next(err);
    }
};


// Delete an item from the watchlist of the current user
exports.deleteWatchlist = async (req, res, next) => {
    try {
        const result = await businessLogic.deleteWatchlist(req, res);
        await redisCluster.del(`${redisKey}${req.headers.userid}`);
        // let apiResponse = response.generate(1, "Delete successful");
        if (result) {
            res.status(200).send({
                "code":1,
                "message":"Delete successful"
              });
        }

    } catch (err) {
        if (!err.statusCode) {
            err.message = "Item couldn't be deleted (DB delete failure)";
            err.statusCode = 500;
        }
        next(err);
    }
};


// Format the data as per response needed
dataProjection = (data) => {
    data = JSON.parse(data);
    let formattedData = data.Items.map(({ AssetId, AssetType, Duration, Date }) => ({
        id: AssetId,
        asset_type: AssetType,
        duration: Duration,
        date: Date,
    }));
    return formattedData;
};
