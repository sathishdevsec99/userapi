const bcrypt = require('bcrypt');
const saltRounds = 10;
const _ = require('lodash');

module.exports = {

    verifyPassword : async(oldPassword,PasswordHash) => {
        try {
            let match = await bcrypt.compare(oldPassword, PasswordHash);
            if(match){
                return true
            }
            return false
        } catch (err) {
            return {
                status: false,
                code: 500,
                data: err.message
            }
        }
    },
    hashPassword : async(password) => {
        try{
            let hashedPwd = await bcrypt.hash(password, saltRounds);
            return hashedPwd
        } catch (err) {
            return {
                status: false,
                code: 500,
                data: err.message
            }
        }
    },

    hidepassword: async(bodyJson) => {
        if(_.isEmpty(bodyJson)) {
            return null;
        }
        ("new_password" in bodyJson) ? bodyJson.new_password = "--##--##--" : bodyJson;
        ("old_password" in bodyJson) ? bodyJson.old_password = "--##--##--" : bodyJson;
        return bodyJson;
    },
};