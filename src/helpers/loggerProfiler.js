const winston = require('winston');
const console = new winston.transports.Console();
const logConfiguration = {
    'transports': [
        console
    ]
}
const logger = winston.createLogger(logConfiguration);

if(process.env.SPEED_PROFILE !=1){
    logger.remove(console)
}

const startProfile =()=>{
    if(process.env.SPEED_PROFILE !=1){
        return ;
    }
   return logger.startTimer()
}

const doneProfile =(object,msg,level='info')=>{
    if(process.env.SPEED_PROFILE !=1){
        return ;
    }
    return object.done({ message: msg,level:level})
}

module.exports={
    startProfile:startProfile,
    doneProfile:doneProfile
}
 

