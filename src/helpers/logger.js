const winston = require('winston');

const config = require('../config/config');
const appDir = require('./rootDirectory');

const level = () => {
  const env = (process.env.NODE_ENV).trim() || 'dev';
  const isDevelopment = env === 'dev'
  return isDevelopment ? 'debug' : 'warn'
}

winston.addColors(config.wistonColor);

//log format
const format = winston.format.combine(
  winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
  winston.format.colorize({ all: true }),
  winston.format.printf(
    (info) => `${info.timestamp} ${info.level}: ${info.message}`,
  ),
)


const transports = [
  new winston.transports.Console(),
  // new winston.transports.File({
  //   filename: `${appDir}/logs/${process.env.ERROR_LOG}`,
  //   level: `${process.env.WISTON_LEVEL}`,
  // }),
  // new winston.transports.File({ filename: `${appDir}/logs/${process.env.ALL_LOG}` }),
]

const Logger = winston.createLogger({
  level: ['info'],
  levels:config.wistonLevels,
  format,
  transports,
})


module.exports=Logger